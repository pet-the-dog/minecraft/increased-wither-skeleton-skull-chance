# Increased Wither Skeleton Skull Drops Datapack

A simple datapack to increase the drop chance of wither skeleton skulls.

See the [Modrinth](https://modrinth.com/project/increased-wither-skeleton-skull-drops) page for more information.